#**********************************************
#
#		Chiffrement post-Quantique
#
#
#
#
# Project créé par  Alexandre Ansel
#                   Alexandre Azzaro
#                   Gulian Dragon
#
# Le 25/02/2020
#
# Programme de lancement des algorithmes de chiffrement
#
#**********************************************

import sys
import os.path
from pathlib import Path
import pickle
import shutil

from PySide2.QtWidgets import QMainWindow, QApplication, QListWidget, QListWidgetItem, QMessageBox, QFileDialog, QLineEdit, QPushButton
from PySide2.QtCore import Qt, QModelIndex, QUrl
from PySide2.QtGui import QIcon

from ChiffrementPostQuantiqueUI import Ui_MainWindow

class MainWindow(QMainWindow):
    
    lstModele = list()

    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        #self.ajoutMethodeChiffrement(self.ui.tableauModeleExistant, QListWidgetItem("RSA"), QListWidgetItem("ECDSA"), QListWidgetItem("Sphincs+"))

        self.ui.tableauModeleExistant.itemDoubleClicked.connect(self.selectionMethodeChiffrementAdd)
        self.ui.tableauModeleSelectionne.itemDoubleClicked.connect(self.selectionMethodeChiffrementRemove)

        self.ui.pushButtonNewModele.clicked.connect(self.nouveauModele)
        self.ui.pushButtonRename.clicked.connect(self.renommerModele)
        self.ui.pushButtonDelModele.clicked.connect(self.supprimerModele)

        self.ui.pushButtonNewDossier.clicked.connect(self.nouveauDossier)
        self.ui.pushButtonDelDossier.clicked.connect(self.supprimerDossier)

        self.ui.boutonLancement.clicked.connect(self.debutBenchmark)

        self.initialisation()

	# Retour de commande pour les boutons du menu
	#self.ui.menuFichier.triggered.connect(self.Methode)


	# Retour de commande pour les boutons de l'interface
	#self.ui.monBouton.clicked.connect(self.Methode)
	#self.ui.checkBox.stateChanged.connect(self.Methode)


    def initialisation(self):
        """
        Initialisation du programme en chargeant les modèles qui ont été chargés précédemment.
        """

        print(getProjectPath())
        
        try:
            modeles = pickle.load(open("modele", "rb"))
        except FileNotFoundError :
            return

        if len(modeles) != 0:
            
            for modele in modeles:
                nomModel, _ = modele
                self.ajoutModeleChiffrementPath(modele)
                self.ajoutMethodeChiffrementTable(self.ui.tableauModeleExistant, nomModel)

        
        print(self.lstModele)


    def nouveauModele(self, path='none'):
        """
        Méthode appelée lorsqu'on veut ajouter un modèle dans le tableau des modèles existants
        """

        chemin, _ = QFileDialog.getOpenFileName()

        #if path != 'none':
        #    chemin, _ = QFileDialog.getOpenFileName(dir=path)
        #else:
        #    chemin, _ = QFileDialog.getOpenFileName()

        modele = Path(chemin).stem      # Ne garde que le nom du fichier sans l'extension
        self.ajoutModeleChiffrementPath((modele, str(chemin)))
        self.ajoutMethodeChiffrementTable(self.ui.tableauModeleExistant, modele)

    def renommerModele(self):
        """
        Méthode appelée lorsqu'on veut modifier le nom d'un modèle
        """

        nouveauNom = self.ui.stringBox.text()
        
        if nouveauNom == "":
            self.ui.stringBox.setText("Modifier le nom ici")
            return 0

        item = self.ui.tableauModeleExistant.currentItem()
        nomModele = item.text()

        for modele in self.lstModele:
            nom, path = modele

            if nom == nomModele:
                self.lstModele.remove(modele)
                self.lstModele.append((nouveauNom, path))

        row = self.ui.tableauModeleExistant.currentRow()
        self.ui.tableauModeleExistant.takeItem(row)
        self.ajoutMethodeChiffrementTable(self.ui.tableauModeleExistant, nouveauNom)
        self.ui.stringBox.setText("")

    def supprimerModele(self):
        """
        Méthode appelée lorsqu'on veut supprimer un modèle dans le tableau des modèles existants
        /!\ On ne peut supprimer que les modèles faisant parti de la liste des modèles existants
        """

        if self.ui.tableauModeleExistant.count() <= 0:
            return

        try:
            itemRow = self.ui.tableauModeleExistant.currentRow()
            modele = self.ui.tableauModeleExistant.currentItem().text()
        except AttributeError:
            return

        self.ui.tableauModeleExistant.takeItem(itemRow)

        for element in self.lstModele:
            nomModele, pathModele = element
            if nomModele == modele:
                '''
                passage en commentaire de la suppression du modèle
                '''
                # os.rmdir(pathModele)
                self.lstModele.remove(element)
                
        self.sauvegardeDuLstModele()

    def nouveauDossier(self):
        """
        Méthode appelée lorsqu'on souhaite ajouter un dossier dans le dossier du projet
        Appel la méthode self.nouveauModele
        """

        chemin = QFileDialog.getExistingDirectoryUrl()

        path = chemin.path()

        if path == '':
            return 0

        nomDossier = path.split('/')
        nomDossier = nomDossier[-1]

        if getOS() == 1:
            path = path.replace('/','',1)
            print(path)

        destination = getProjectPath()+"/"+nomDossier

        print("destination : ", destination)
        print("from : ", path)

        route = shutil.copytree(path, destination)

        self.nouveauModele(path=route)

        pass

    def supprimerDossier(self):
        """
        Méthode appelée lorsqu'on souhaite supprimer un dossier dans le dossier du projet
        """
        pathModele = self.getPathFromItem(self.ui.tableauModeleExistant.currentItem())

        pathModeleDecompose = pathModele

        pathModeleDecompose = pathModeleDecompose.split('/')

        directoryProject = getProjectPath().split('/')[-1]

        pathToRemove = list()
        index = 0

        while(True):

            pathToRemove.append(pathModeleDecompose[index])
            index += 1

            if pathModeleDecompose[index] == directoryProject:
                pathToRemove.append(pathModeleDecompose[index])
                pathToRemove.append(pathModeleDecompose[index+1])
                pathToRemove = '/'.join(pathToRemove)
                break


        flag = 1
        
        for index, modele in enumerate(self.lstModele):
            _, path = modele
            if pathToRemove in path:
                if path != pathModele:
                    flag = 0
                    break

        self.supprimerModele()



        if flag == 1 :
            
            message = QMessageBox(QMessageBox.Icon.Question, "confirmation de suppression", "Etes-vous sur de vouloir supprimer le chemin suivant :" + pathToRemove)
            message.setStandardButtons(QMessageBox.Yes|QMessageBox.No)
            reponse = message.exec()
            
            if reponse == QMessageBox.Yes:
                try:
                    shutil.rmtree(pathToRemove)
                except:
                    raise NotImplementedError("erreur de suppression")
                    
                message = QMessageBox(QMessageBox.Icon.Information,"Information de suppression", "Dossier supprimé", QMessageBox.StandardButton.NoButton, self)
                message.open()
        else: 
            message = QMessageBox(QMessageBox.Icon.Information,"Information de suppression", "Dossier non supprimé, le dossier est utilisé par un autre modèle\nLe modèle a été supprimé", QMessageBox.StandardButton.NoButton, self)
            message.open()


    def selectionMethodeChiffrementAdd(self):
        """
        Méthode appelée lors d'un double clique sur un item de la liste des modèles existants
        """

        provenance = self.ui.tableauModeleExistant
        destination = self.ui.tableauModeleSelectionne

        itemSelectionne = provenance.currentItem()
        rowItem = provenance.indexFromItem(itemSelectionne)
        provenance.takeItem(rowItem.row())
        self.ajoutMethodeChiffrementTable(destination, itemSelectionne.text())
        
        
    def selectionMethodeChiffrementRemove(self):
        """
        Méthode appelée lors d'un double clique sur un item de la liste des modèles selectionnés
        """

        provenance = self.ui.tableauModeleSelectionne
        destination = self.ui.tableauModeleExistant

        itemSelectionne = provenance.currentItem()
        rowItem = provenance.indexFromItem(itemSelectionne)
        provenance.takeItem(rowItem.row())
        self.ajoutMethodeChiffrementTable(destination, itemSelectionne.text())
        #self.ajoutMethodeChiffrementTable()
        

    def ajoutMethodeChiffrementTable(self, table, *elements):
        """
        Ajoute la methode de chiffrement à la table passée en paramètre
        :param table: Table où ajouter la méthode
        :type table: QListWidget
        :type elements: str
        """

        if type(table) != QListWidget:
            raise TypeError("Le type de table n'est pas un QListWidget")

        for element in elements:

            if type(element) != str:
                raise TypeError( element, "Le type de l'élément n'est pas un string")
            
            if element not in self.itemDansLaTable(self.ui.tableauModeleExistant, self.ui.tableauModeleSelectionne):
                table.addItem(element)

    def ajoutModeleChiffrementPath(self, *elements : tuple):
        """
        Ajoute les modèles passés en argument au path
        :type *element: Tuple
        """

        for elt in elements:
            
            if type(elt) != tuple:
                raise TypeError( elt, "Le type de l'élément n'est pas un tuple")

            if elt not in self.lstModele:
                self.lstModele.append(elt)
        
        self.sauvegardeDuLstModele()

    def debutBenchmark(self):
        """
        lancement du benchmark
        """

        self.sauvegardeDuLstModele()
        nombreMethode = self.ui.tableauModeleSelectionne.count()
        methodeSeletionnee = list()
        
        methodeSeletionnee = self.itemDansLaTable(self.ui.tableauModeleSelectionne)
            
        print("modèles sélectionnés :", methodeSeletionnee)


    def sauvegardeDuLstModele(self):
        """
        Sauvegarde self.lstModele dans le fichier
        """

        pickle.dump(self.lstModele, open("modele", "wb"))


    
    def itemDansLaTable(self, *tables):
        """
        itemDansLaTable(self, *tables) -> list(str)
        retourne le nom des items dans la table passée en paramètre
        """

        itemsDuTableau = list()
        
        for table in tables:

            if type(table) != QListWidget:
                raise TypeError("L'élement passé n'est pas une table")
            

            for i in range(table.count()):
                itemsDuTableau.append(table.item(i).text())
        
        return itemsDuTableau

    def getPathFromItem(self, item):
        """
        getPathFromItem(item : QListWidgetItem) -> str
        return le chemin du modele s'il existe sinon ''
        """

        if type(item) != QListWidgetItem:
            raise TypeError("L'argument passé n'est pas un QListWidgetItem")

        nomModele = item.text()

        for elmt in self.lstModele:
            nom, path = elmt
            if nom == nomModele:
                return path
        
        return ''

        pass


def getProjectPath():
    """
    getProjectPath -> str

    Retourne le path du projet en cours
    /!\ peut poser des problèmes
    """
    try:

        if getOS() == 1:
            # Si on utilise windows
            return os.path.dirname(sys.modules['__main__'].__file__)
        elif getOS() == 0:
            # Si on utilise Linux
            path = os.path.realpath(sys.argv[0])
            path = path.split('/')
            path.pop(-1)
            path = '/'.join(path)
            return path 
    except:
        raise NotImplementedError("Erreur dans la lecture du chemin du fichier en cours")
    
def getOS():
    """
    getOS() -> str
    return 0 for linux or 1 for windows
    """
    if os.environ.get('OS') == None:
        return 0
    else:
        return 1

def reformulePath(path):
    """
    reformule le path dans une forme acceptable
    reformulePath -> str
    """
    if path[0] == '/':
        path = path.replace('/', '', 1)

    return path




if __name__ == "__main__":

    app = QApplication(sys.argv)

    window = MainWindow()

    window.show()

    sys.exit(app.exec_())
