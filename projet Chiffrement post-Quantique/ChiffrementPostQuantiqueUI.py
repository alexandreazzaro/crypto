# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ChiffrementPostQuantiqueUI.ui'
##
## Created by: Qt User Interface Compiler version 5.14.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QMetaObject, QObject, QPoint,
    QRect, QSize, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QLinearGradient, QPalette, QPainter, QPixmap,
    QRadialGradient)
from PySide2.QtWidgets import *


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(997, 794)
        self.actionMod_le = QAction(MainWindow)
        self.actionMod_le.setObjectName(u"actionMod_le")
        self.actionMod_le.setEnabled(False)
        self.actionMod_le.setVisible(True)
        self.actionAjouter_mod_le = QAction(MainWindow)
        self.actionAjouter_mod_le.setObjectName(u"actionAjouter_mod_le")
        self.actionRenommer_mod_le = QAction(MainWindow)
        self.actionRenommer_mod_le.setObjectName(u"actionRenommer_mod_le")
        self.actionSupprimer_mod_le = QAction(MainWindow)
        self.actionSupprimer_mod_le.setObjectName(u"actionSupprimer_mod_le")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.gridLayoutWidget = QWidget(self.centralwidget)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(50, 20, 691, 511))
        self.gridLayout = QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.tableauModeleExistant = QListWidget(self.gridLayoutWidget)
        self.tableauModeleExistant.setObjectName(u"tableauModeleExistant")

        self.gridLayout.addWidget(self.tableauModeleExistant, 1, 0, 1, 1)

        self.tableauModeleSelectionne = QListWidget(self.gridLayoutWidget)
        self.tableauModeleSelectionne.setObjectName(u"tableauModeleSelectionne")

        self.gridLayout.addWidget(self.tableauModeleSelectionne, 1, 1, 1, 1)

        self.spinBox = QSpinBox(self.gridLayoutWidget)
        self.spinBox.setObjectName(u"spinBox")

        self.gridLayout.addWidget(self.spinBox, 2, 1, 1, 1)

        self.label = QLabel(self.gridLayoutWidget)
        self.label.setObjectName(u"label")
        self.label.setAlignment(Qt.AlignCenter)

        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)

        self.label_2 = QLabel(self.gridLayoutWidget)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setAlignment(Qt.AlignCenter)

        self.gridLayout.addWidget(self.label_2, 0, 1, 1, 1)

        self.label_3 = QLabel(self.gridLayoutWidget)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setAlignment(Qt.AlignCenter)

        self.gridLayout.addWidget(self.label_3, 2, 0, 1, 1)

        self.spinBox_2 = QSpinBox(self.gridLayoutWidget)
        self.spinBox_2.setObjectName(u"spinBox_2")

        self.gridLayout.addWidget(self.spinBox_2, 3, 1, 1, 1)

        self.label_4 = QLabel(self.gridLayoutWidget)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setAlignment(Qt.AlignCenter)

        self.gridLayout.addWidget(self.label_4, 3, 0, 1, 1)

        self.boutonLancement = QPushButton(self.centralwidget)
        self.boutonLancement.setObjectName(u"boutonLancement")
        self.boutonLancement.setGeometry(QRect(680, 610, 93, 28))
        self.stringBox = QLineEdit(self.centralwidget)
        self.stringBox.setObjectName(u"stringBox")
        self.stringBox.setGeometry(QRect(40, 680, 460, 22))
        self.gridLayoutWidget_2 = QWidget(self.centralwidget)
        self.gridLayoutWidget_2.setObjectName(u"gridLayoutWidget_2")
        self.gridLayoutWidget_2.setGeometry(QRect(40, 580, 394, 80))
        self.gridLayout_2 = QGridLayout(self.gridLayoutWidget_2)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.pushButtonDelModele = QPushButton(self.gridLayoutWidget_2)
        self.pushButtonDelModele.setObjectName(u"pushButtonDelModele")

        self.gridLayout_2.addWidget(self.pushButtonDelModele, 0, 2, 1, 1)

        self.pushButtonNewModele = QPushButton(self.gridLayoutWidget_2)
        self.pushButtonNewModele.setObjectName(u"pushButtonNewModele")

        self.gridLayout_2.addWidget(self.pushButtonNewModele, 0, 0, 1, 1)

        self.pushButtonRename = QPushButton(self.gridLayoutWidget_2)
        self.pushButtonRename.setObjectName(u"pushButtonRename")

        self.gridLayout_2.addWidget(self.pushButtonRename, 0, 1, 1, 1)

        self.pushButtonNewDossier = QPushButton(self.gridLayoutWidget_2)
        self.pushButtonNewDossier.setObjectName(u"pushButtonNewDossier")

        self.gridLayout_2.addWidget(self.pushButtonNewDossier, 1, 0, 1, 1)

        self.pushButtonDelDossier = QPushButton(self.gridLayoutWidget_2)
        self.pushButtonDelDossier.setObjectName(u"pushButtonDelDossier")

        self.gridLayout_2.addWidget(self.pushButtonDelDossier, 1, 2, 1, 1)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 997, 26))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.actionMod_le.setText(QCoreApplication.translate("MainWindow", u"Mod\u00e8le", None))
        self.actionAjouter_mod_le.setText(QCoreApplication.translate("MainWindow", u"Ajouter mod\u00e8le", None))
        self.actionRenommer_mod_le.setText(QCoreApplication.translate("MainWindow", u"Renommer mod\u00e8le", None))
        self.actionSupprimer_mod_le.setText(QCoreApplication.translate("MainWindow", u"Supprimer mod\u00e8le", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"<html><head/><body><p><span style=\" font-size:14pt;\">Mod\u00e8les Existants</span></p></body></html>", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"<html><head/><body><p><span style=\" font-size:14pt;\">Mod\u00e8les S\u00e9lectionn\u00e9s</span></p></body></html>", None))
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"Taille de la cl\u00e9", None))
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"TextLabel", None))
        self.boutonLancement.setText(QCoreApplication.translate("MainWindow", u"Lancer", None))
        self.pushButtonDelModele.setText(QCoreApplication.translate("MainWindow", u"Supprimer un mod\u00e8le", None))
        self.pushButtonNewModele.setText(QCoreApplication.translate("MainWindow", u"Ajouter un mod\u00e8le", None))
        self.pushButtonRename.setText(QCoreApplication.translate("MainWindow", u"Renommer le mod\u00e8le", None))
        self.pushButtonNewDossier.setText(QCoreApplication.translate("MainWindow", u"Ajouter un dossier", None))
        self.pushButtonDelDossier.setText(QCoreApplication.translate("MainWindow", u"Supprimer un dossier", None))
    # retranslateUi

